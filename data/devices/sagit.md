---
name: "Xiaomi Mi 6"
deviceType: "phone"
portType: "Halium 9.0"
image: "https://wiki.lineageos.org/images/devices/sagit.png"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "-"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "dualSim"
        value: "+"
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "-"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "sdCard"
        value: "x"
      - id: "rtcTime"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "+"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "-"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "x"

deviceInfo:
  - id: "arch"
    value: "Arm64"
  - id: "cpu"
    value: "Quad-core 2.45 GHz Kryo 280 & Quad-core 1.9 GHz Kryo 280"
  - id: "chipset"
    value: "Qualcomm MSM8998 Snapdragon 835"
  - id: "gpu"
    value: "Adreno 540"
  - id: "rom"
    value: "64/128GB"
  - id: "ram"
    value: "4/6GB"
  - id: "android"
    value: "Android 7.1 Nougat"
  - id: "battery"
    value: "Non-removable Li-Po 3350 mAh (QC 3.0)"
  - id: "display"
    value: "1080 x 1920 pixels, 5.15 inches (~428 ppi pixel density)"
  - id: "rearCamera"
    value: "Dual 12 MP, f/2.0, phase detection autofocus, dual-LED (dual tone) flash"
  - id: "frontCamera"
    value: "5 MP (No flash)"
  - id: "dimensions"
    value: "70,49 x 145,17 x 7,45 (length x width x height, mm.)"
  - id: "weight"
    value: "168g"

contributors:
  - name: "Verevka"
    photo: "https://github.com/UbuntuTouch-sagit/ubuntu-touch-sagit/releases/download/v1.1/verevka.jpeg"
    forum: "https://forums.ubports.com/user/verevka"

externalLinks:
  - name: "Forum Post"
    link: "https://forums.ubports.com/topic/4671/ubuntu-touch-halium9-0-xiaomi-mi6-sagit"
    icon: "yumi"
  - name: "Repository: Xiaomi Mi 6"
    link: "https://github.com/UbuntuTouch-sagit"
    icon: "github"
  - name: "Repository: Xiaomi Mi 6 Gitlab CI"
    link: "https://gitlab.com/ubports/community-ports/android9/xiaomi-mi-6"
    icon: "gitlab"

seo:
  description: "Switch your Xiaomi Mi 6, as your open source daily driver OS."
  keywords: "Ubuntu Touch, Xiaomi Mi 6, sagit, Linux on Mobile"
---
